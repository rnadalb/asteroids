package org.example.asteroides;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Vector;

import android.content.Context;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;

public class MagatzemPuntacioFitxerExtern implements MagatzemPuntacio {

	private static String fitxer = Environment.getExternalStorageDirectory()+ "/puntuaciones.txt";
	private Context context;
	
	public MagatzemPuntacioFitxerExtern(Context contexto)
	{ context=contexto;}
	
	public void guardaPuntuacio(int punts, String nom, long milis) {
		guardarPuntuacion(punts, nom, getDateFromMilis(milis));
	}

	public void guardarPuntuacion(int punts, String nom, String sData) {
		String estatSD=Environment.getExternalStorageState();
		if(estatSD.equals(Environment.MEDIA_MOUNTED))
		{
			try {
				FileOutputStream f= new FileOutputStream(fitxer, true);
				Puntuacio p=new Puntuacio(nom, punts, sData);
				String text=p.converteixString()+"\n";
				f.write(text.getBytes());
				f.close();
			} catch (FileNotFoundException e) {
				Log.e(MainActivity.APP_TAG,"Error obrint fitxer:" + fitxer + e.getStackTrace());
			} catch (IOException e) {
				Log.e(MainActivity.APP_TAG,"Error escrivint fitxer:" + fitxer + e.getStackTrace());
			}
		}
		else
		{
			Log.d(MainActivity.APP_TAG,"No es pot escriure en emmagatzematge extern");
		}
	}

	
	public Vector<Puntuacio> llistaPuntuacions(int cantidad) {
		Vector<Puntuacio> result=new Vector<Puntuacio>();
		String estadoSD=Environment.getExternalStorageState();
		if(estadoSD.equals(Environment.MEDIA_MOUNTED) ||
				estadoSD.equals(Environment.MEDIA_MOUNTED_READ_ONLY))
		{
			FileInputStream f;
			try {
				f = new FileInputStream(fitxer);
	
				BufferedReader entrada=new BufferedReader(new InputStreamReader(f));
				int n=0;
				String linia=null;
				do
				{
					linia=entrada.readLine();
	
					if(linia!=null)
					{
						result.add(new Puntuacio(linia));
						n++;
					}
	
				}
				while(n<cantidad && linia!=null);
				f.close();
				 
			}
			catch (FileNotFoundException e) {
				Log.e(MainActivity.APP_TAG, "Error obrint fitxer:" + fitxer + e.getStackTrace());
			} 
			catch (IOException e) {
				Log.e(MainActivity.APP_TAG, "Error escrivint fitxer:" + fitxer + e.getStackTrace());
			}
			Collections.sort(result);
		}
		else
		{
			Log.d(MainActivity.APP_TAG, "No es pot escriure en emmagatzematge extern");
		}
		return result;
	}
	
	public static String getDateFromMilis(long milliSeconds)
	{return getDateFromMilis(milliSeconds,"dd/MM/yyyy");	}
	
	public static String getDateFromMilis(long milliSeconds, String dateFormat)
	{     return  DateFormat.format("dd/MM/yyyy", milliSeconds).toString();	}

}
