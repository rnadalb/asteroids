package org.example.asteroides;

import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MagatzemPuntacioSQLite extends SQLiteOpenHelper implements
		MagatzemPuntacio {

	public MagatzemPuntacioSQLite(Context context) {
		super(context, "puntuaciones", null, 5);
	
	}

	public void guardaPuntuacio(int puntos, String nombre, long milis) {
		guardarPuntuacion(puntos, nombre, MagatzemPuntacioArray.getDateFromMilis(milis));
		
	}

	Boolean bTrucos=false;
	public void guardarPuntuacion(int puntos, String nombre, String sFecha) {
		SQLiteDatabase db=getWritableDatabase();
		db.execSQL("INSERT INTO puntuaciones VALUES (null, "+
				puntos+",'"+nombre+"','"+sFecha+"')");
		
		if(bTrucos)
		{
			ContentValues nuevoRegistro=new ContentValues();
			nuevoRegistro.put("punts",puntos+100);
			nuevoRegistro.put("nom", "pp");
			nuevoRegistro.put("data", sFecha);
			db.insert("puntuaciones",null,nuevoRegistro);
			
			db.update("puntuaciones", nuevoRegistro, "nom='?'", new String[]{"pp"});
			
			db.delete("puntuaciones","nom='enemigo'",null);
		}
	}

	public Vector<Puntuacio> llistaPuntuacions(int cantidad) {
		Vector<Puntuacio> result=new Vector<Puntuacio>();
		SQLiteDatabase db=getReadableDatabase();
		Cursor cursor=db.rawQuery("SELECT punts,  nom,data FROM "+
				"puntuaciones ORDER BY punts DESC LIMIT "+cantidad,null);
//		Cursor cursor2=db.query
//				("puntuaciones", 
//				new String[]{"punts",  "nom","data"},
//				null, // where con ?
//				null, // where argumentos 
//				null, // group by 
//				null, // having
//				"punts",
//				Integer.toString(cantidad));
	
		while(cursor.moveToNext())
			result.add(new Puntuacio(cursor.getString(1), // Nombre
					cursor.getInt(0), //punts
					cursor.getString(2) // data
					));
		cursor.close();
		return result;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE puntuaciones ("+
				"_id INTEGER PRIMARY KEY AUTOINCREMENT,"+
				"punts INTEGER,nom TEXT,data TEXT)");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		switch(oldVersion)
		{
		case 1:
			//db.execSQL("ALTER TABLE puntuaciones...")
			break;
		case 2:
			//db.execSQL("CREATE TABLE puntuaciones2...")
			break;
		}
		
	}

}
