package org.example.asteroides;

import android.util.Log;

public class Puntuacio implements Comparable {

	
	public String nom;
	public int punts;
	public String data;
	
	public Puntuacio()
	{	}
	
	public Puntuacio(String nom, int punts, String data)
	{
		this.nom = nom;
		this.punts = punts;
		this.data = data;
	}
	
	public Puntuacio(String cadena)
	{
		if(cadena.indexOf(separador)==-1)
		{
			Log.e("asteroides","Formato erroneo");
			return;
		}
		String []campos=cadena.split(separador);
		if(campos.length<3)
		{
			Log.e("asteroides","Formato erroneo");
			return;
		}
		try
		{
		punts =Integer.valueOf(campos[0]);
		
		}
		catch(Exception ex)
		{
			Log.e("asteroides","Error en la conversion");
			punts =-7;
		}
		nom =campos[1];
		data =campos[2];
	}
	

	
	public int compareTo(Object another) {
		Puntuacio altre=(Puntuacio)another;
		return -Integer.valueOf(this.punts).compareTo(altre.punts);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String separador=";";
	public String converteixString()
	{
		return this.punts +separador+this.nom +separador+this.data;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Nombre: "+ nom +" punts:"+ punts +" en "+ data;
	}
	

}
