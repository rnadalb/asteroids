package org.example.asteroides;

import java.util.Vector;

public interface MagatzemPuntacio {

	public void guardaPuntuacio(int puntos, String nombre, long milis);
	public Vector<Puntuacio> llistaPuntuacions(int cantidad);
}