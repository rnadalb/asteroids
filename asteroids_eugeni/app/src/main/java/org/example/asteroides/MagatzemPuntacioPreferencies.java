package org.example.asteroides;

import java.util.Vector;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.format.DateFormat;

public class MagatzemPuntacioPreferencies implements MagatzemPuntacio {

	private static String PREF_PUNTUACIO ="puntuaciones";
	private Context context;
	
	public MagatzemPuntacioPreferencies(Context contexto)
	{this.context=contexto;}
	
	public void guardaPuntuacio(int puntos, String nombre, long milis) {
		guardarPuntuacion(puntos, nombre, getDateFromMilis(milis));
	}

	public void guardarPuntuacion(int puntos, String nombre, String sFecha) {
		SharedPreferences preferencies = context.getSharedPreferences(PREF_PUNTUACIO, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferencies.edit();
		Puntuacio p = new Puntuacio(nombre, puntos, sFecha);
		editor.putString("puntuacion", p.converteixString());
		editor.commit();
	}

	public Vector<Puntuacio> llistaPuntuacions(int cantidad) {
		Vector<Puntuacio> result=new Vector<Puntuacio>();
		SharedPreferences preferencias = context.getSharedPreferences(PREF_PUNTUACIO, Context.MODE_PRIVATE);
		String s=preferencias.getString("puntuacion", "");
		if(s!="")
		{
			Puntuacio p=new Puntuacio(s);
			result.add(p);
//			// JAV
////			int iSeparador1=s.indexOf('|');
////			int iSeparador2=s.indexOf(s,iSeparador1);
////			String sPuntos=s.substring(0,iSeparador1);
////			String sNombre=s.substring(iSeparador1, iSeparador2-iSeparador1);
////			String sFecha=s.substring(iSeparador2);
////			int punts=Integer.valueOf(sPuntos);
////			Puntuacion p=new Puntuacion(sNombre, punts, sFecha);
////			result.add(p);
//			// CESAR
//			String []campos=s.split("|");
//			Puntuacion p=new Puntuacion(campos[1], Integer.valueOf(campos[0]), campos[2]);

		}
			
	
		return result;
	}

	public static String getDateFromMilis(long milliSeconds)
	{return getDateFromMilis(milliSeconds,"dd/MM/yyyy");	}
	
	public static String getDateFromMilis(long milliSeconds, String dateFormat)
	{
	    // Create a DateFormatter object for displaying date in specified format.
//	    SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
//
//	    // Create a calendar object that will convert the date and time value in milliseconds to date. 
//	     Calendar calendar = Calendar.getInstance();
//	     calendar.setTimeInMillis(milliSeconds);
//	     return formatter.format(calendar.getTime());
	     
	     return  DateFormat.format("dd/MM/yyyy", milliSeconds).toString();
	}
}
