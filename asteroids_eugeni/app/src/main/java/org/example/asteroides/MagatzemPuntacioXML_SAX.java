package org.example.asteroides;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlSerializer;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

public class MagatzemPuntacioXML_SAX implements MagatzemPuntacio {

	private static String FICHERO = "puntuaciones.xml";
	private Context context;
	private boolean cargadaLista;

	Vector<Puntuacio> lista;

	public MagatzemPuntacioXML_SAX(Context contexto) {
		this.context = contexto;
		lista = new Vector<Puntuacio>();
		cargadaLista = false;
	}

	public void guardarPuntuacion(int puntos, String nombre, String fecha) {
		try{
			if(!cargadaLista){
				leerXML(context.openFileInput(FICHERO));
			}
		}catch (FileNotFoundException e){
		}catch (Exception e) {
			Log.e("Asteroides", e.getMessage(), e);
		}
		lista.add(new Puntuacio(nombre, puntos, fecha));
		try{
			escribirXML(context.openFileOutput(FICHERO, Context.MODE_PRIVATE));
		}catch (Exception e){
			Log.e("Asteroides", e.getMessage(), e);
		}
	}

	public Vector<Puntuacio> llistaPuntuacions(int cantidad) {
		try {
			if (!cargadaLista) {
				leerXML(context.openFileInput(FICHERO));
			}
		} catch (Exception e) {
			Log.e("Asteroides", e.getMessage(), e);
		}

		return lista;
	}

	private void leerXML(InputStream entrada) throws Exception {
		SAXParserFactory fabrica = SAXParserFactory.newInstance();
		SAXParser parser = fabrica.newSAXParser();
		XMLReader lector = parser.getXMLReader();
		ManejadorXML manejadorXML = new ManejadorXML();
		lector.setContentHandler(manejadorXML);
		lector.parse(new InputSource(entrada));
		cargadaLista = true;
	}

	private void escribirXML(OutputStream salida) {
		XmlSerializer serializador = Xml.newSerializer();
		try {
			serializador.setOutput(salida, "UTF-8");
			serializador.startDocument("UTF-8", true);
			serializador.startTag("", "lista_puntuaciones");
			for (Puntuacio puntuacio : lista) {
				serializador.startTag("", "puntuacion");
				serializador.attribute("", "data",
						String.valueOf(puntuacio.data));
				serializador.startTag("", "nom");
				serializador.text(puntuacio.nom);
				serializador.endTag("", "nom");
				serializador.startTag("", "punts");
				serializador.text(String.valueOf(puntuacio.punts));
				serializador.endTag("", "punts");
				serializador.endTag("", "puntuacion");
			}
			serializador.endTag("", "lista_puntuaciones");
			serializador.endDocument();
		} catch (Exception e) {
			Log.e("Asteroides", e.getMessage(), e);
		}
	}

	class ManejadorXML extends DefaultHandler {
		private StringBuilder cadena;
		private Puntuacio puntuacio;

		@Override
		public void startDocument() throws SAXException {
			lista = new Vector<Puntuacio>();
			cadena = new StringBuilder();
		}

		@Override
		public void startElement(String uri, String nombreLocal,
				String nombreCualif, Attributes atr) throws SAXException {
			cadena.setLength(0);
			if (nombreLocal.equals("puntuacion")) {
				puntuacio = new Puntuacio();
				puntuacio.data = String.valueOf(atr.getValue("data"));
			}
		}

		@Override
		public void characters(char[] ch, int comienzo, int lon)
				throws SAXException {
			cadena.append(ch, comienzo, lon);
		}

		@Override
		public void endElement(String uri, String nombreLocal,
				String nombreCualif) throws SAXException {
			if (nombreLocal.equals("punts")) {
				puntuacio.punts = Integer.parseInt(cadena.toString());
			} else if (nombreLocal.equals("nom")) {
				puntuacio.nom = cadena.toString();
			} else if (nombreLocal.equals("puntuacion")) {
				lista.add(puntuacio);
			}
		}

		@Override
		public void endDocument() throws SAXException {

		}
	}

	public void guardaPuntuacio(int puntos, String nombre, long milis) {
		guardarPuntuacion(puntos, nombre,
				MagatzemPuntacioArray.getDateFromMilis(milis));

	}
}
